const router = require('express').Router();
const Client = require('../model/Client');

router.post('/register', async (req, res) => {
    const client = new Client({
        name: req.body.name,
        surname: req.body.surname,
        firstPhone: req.body.firstPhone,
        secondPhone: req.body.secondPhone,
        email: req.body.email,
        character: req.body.character,
        password: req.body.password
    });
    try {
        const savedClient = await client.save();
        res.send(savedClient);
    } catch (error) {
        res.status(400).send(error);
    }
});

module.exports = router;