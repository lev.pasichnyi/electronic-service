const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

// Import routes
const authRoute = require('./routes/auth')

// Authorization to MongoDB, pending connection
mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true });

// check database connection, opening
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('We are connected!');
});

//Middleware
app.use(express.json());

// Route middleware
app.use('/api/client', authRoute);

app.listen(3000, () => console.log('Server up and running'));