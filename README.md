# electronic-service

A useful project for repair services and DIY internet shops

## Getting Started

The whole project process is displayed on public Trello board:

<a href="https://trello.com/b/XuRP31tV/service-center" target="_blank">`trello.com/.../service-center`</a>

1. Clone repository
2. Start from console: node index.js or npm start

### Prerequisites

VS Code, node.js, npm, postman, browser, fresh head.

### Installing

All your needs are : Node.js, npm, MongoDB and server.

## Deployment

Usually, I don't worry about deployment, 
just push and deploy to Heroku: $ git push heroku master

## Built With

* [node.js](https://nodejs.org/) - Runtime engine for JS
* [MongoDB](www.mongodb.com) - NoSQL, wide known cloud database with good scale capabilities
* [npm](https://www.npmjs.com/) - Node package manager, wide library of useful stuff
* [express.js](https://expressjs.com/) - fast web framework for Node.js
* [mongoose](https://mongoosejs.com/) - object modeling for Node.js
* [bootstrap](https://getbootstrap.com/) - toolkit for frontend part of the application

## Authors

* **Lev Pasichnyi** - *Initial work* - [lev.pasichnyi](https://gitlab.com/lev.pasichnyi)

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* The main goal of that project is to reduce operating costs for small business 
* Cheers for all service engineers and entrepreneurs
* If you need support, don't hesitate to call or write an email 